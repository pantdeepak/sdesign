#include<iostream> 
using namespace std; 
  
class Point 
{ 
private: 
    int x, y; 
public: 
  Point(int x1, int y1) {cout<<"Ctor"<<endl; x = x1; y = y1; } 
  
    // Copy constructor 
  Point(const Point &p2) {cout<<"CC" << endl; x = p2.x; y = p2.y; } 
  Point(Point &&p2){cout<<"MC"<<endl;}
  Point & operator=(Point p){
    cout<<"= called" <<endl;
}

    int getX()            {  return x; } 
    int getY()            {  return y; } 
}; 
  
Point dummy(){
  Point p(1,1);
  return p;
}
int main() 
{ 
    Point p1(10, 15); // Normal constructor is called here 
    Point p2 = p1; // Copy constructor is called here 
  
    // Let us access values assigned by constructors 
    cout << "p1.x = " << p1.getX() << ", p1.y = " << p1.getY(); 
    cout << "\np2.x = " << p2.getX() << ", p2.y = " << p2.getY() <<endl; 
    Point px(1,1);
    px = dummy();
    return 0; 
}
