#include<memory>
#include<iostream>
using namespace std;

class Foo{
  int* i;

public :
  Foo(int *i):i(i){cout<<"ctor"<<endl;}
  ~Foo(){cout<<"dtor"<<endl;}
};
// foo with unique ptr
class FooUp{
  unique_ptr<int> i;
  
public :
  FooUp(unique_ptr<int> up):i(move(up)){cout<<"ctor"<<endl;}
  ~FooUp(){cout<<"dtor"<<endl;}
};

class FooSp{
  shared_ptr<int> i;
public :
  FooSp(shared_ptr<int> sp):i(sp){cout<<"ctor"<<endl;}
  ~FooSp(){cout<<"dtor"<<endl;}
};

class Bar{
public:
  void method(){cout<<"method"<<endl;}
};

 int main(){
   Foo f(new int);
   unique_ptr<int> ui(new int(10));
   FooUp fup(move(ui));
   //FooUp fup2(make_unique<int>(10)); doesn't work in C++11
   FooSp fsp(make_shared<int>(10));
   shared_ptr<Bar> spf(make_shared<Bar>());
   spf->method(); // spf.method() will not work.
   return 0;
 }
