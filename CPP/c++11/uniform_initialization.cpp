#include<iostream>
#include<vector>
#include<set>
#include<map>
using namespace std;
class Foo{
  int i;
public:
  Foo(){cout<<"No args ctor called"<<endl;}
  Foo(int i):i(i){cout<<"ctor called"<<endl;}
  bool operator <(Foo f2) const { return i<f2.i;}; // must be const func. otherwise ERROR while using set
};


main(){
  Foo fi; // No args default 
  Foo fi2{10}; // with value
  Foo f[5]; // no args called 5 times
  Foo fa[5] {1,2,4,5,6}; // with args called 5
  Foo *fn = new Foo[5]{1,2,3,4,5}; // with args using new define array of objects Foo *fns  = new Foo{1};
  Foo Fa2[5] = {1,2,3,5,6}; // with args 
  vector<Foo> vf {{1},{2},{3},{4}};
  set<Foo> s{{2},{4}};
  map<Foo,int> mp{{{1},2},{{3},4}};
  cout << mp[Foo(1)];
}
 
