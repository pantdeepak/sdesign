/* 

   This file give a basic understanding of what happen when you return a value or pass a value to function.
   What are the copying overhead. is ctor/copy ctor/ move ctor called?

#0 When returning a object or a container RVO is invoked. No move ctor will be called. (neither of container nor of underlying objects)

#1 When you are passing an object which is **rvalue** (temporary) to a function by value (not by reference). You will see the move ctor called.
 
#2 If you are passing a container(say vector) by value,which in itself is a lvalue (not temporary).You will see the underlying class copy ctor called. This scenerio should be avoided due to runtime overhead.

#3 If you are passing a function, rvalue container (say by calling a function who returns a container). The conatainer move ctor is called. But underlying object ctor/copy ctor/ move ctor will not be called
   Hence even if we have to call a move ctor in case of vector  objects, we will only call the move ctor of vector not the underlying objects!. this will save time and no unnecessary copy will happen.

*/


#include<memory>
#include<iostream>
#include<vector>
using namespace std;


class X{
  int val;
public:
  X(int i):val(i){cout<<"constructor X"<< val <<endl;}
  X(){cout<<"constructor X"<< val <<endl;}
  X(const X &a) {cout<<"copy constructor X"<< val <<endl;}
  X(X &&a) {cout<<"move constructor X"<< val <<endl;} // Notice no const move the move ctor
  ~X(){cout<<"Destructor X"<< val << endl;}
  X operator = (X a){ cout<<"Assignment operator"<<endl; return a;} // notice i am using pass by value (not pass by const val or ref. move ctor will be called
  void do1(){cout<<"Do"<<endl;}
};

vector<X> container_copy(){
  vector<X> a1(3);
  return a1;
}

void container_copy_exp(){
  cout<<"--"<<endl;
  vector<X> b;
  cout<<"--"<<endl;
  vector<X> a = container_copy(); // create new from a copy.
  b = a; // no operator = called 
  cout<<"copy Done"<<endl;
}

X return_val_opt(){
  X a;
  return a;
}

void return_val_opt_exp(){
  X b = return_val_opt(); //RVO , no move constructor called.
  X a = move(b); // move const will be called
  //a = b; // assignment operator called. but no move ctor. 
  a = return_val_opt(); // right hand value os r value. move ctro is called.
}

main(){
  //auto_and_unique_ptr();
  cout<<"  container_copy_exp_start(); "<<endl;
  container_copy_exp();
  cout<<"  container_copy_exp_end(); "<<endl;
  return_val_opt_exp();
}

  
