#include<iostream>
#include<cmath>
using namespace std;

class Solution {
public:
  bool close_enough(float a,float b){
    if(abs(a-b)<1E-8 || abs(a-b) < max(a,b)*1E-8)
      return true;
    return false;
  }
    float mySqrt(float x) {
        float start =0;
        float end = x;
        while(!close_enough(start,end)){
            float mid = start + (end - start)/2; // MUST BE LONG.
            if(close_enough(mid * mid,x)){
                return mid;
            }else if(mid*mid <x){
	      start = mid ; // careful about increment the start index 
            }else if(mid*mid > x){
	      end = mid; // careful about decrement the end index.
            }
        }
        return start;
        
    }
};

main(){
  Solution S;
  cout<< S.mySqrt(5.0) <<endl;
  
}
