												+ C++ BASIC FEATURES  +
												++++++++++++++++++++++


1) Allocating/freeing memory in heap
  myclass *p1 = new myclass();
  myclass *p2 = new myclass[10]; //10 object constructed, constructor called 10 times 
  
  delete p1; // free an object
  delete [] p2; // free an array // must use [] to free all 10 objects otherwise will do only one object

1) Make base class functions virtual if you want the derived class function to be called.

class Base{
public:
	virtual foo();
}

class Derived : public Base{

public:
	foo();
}

Base *p = new Derived();
p->foo() // if foo is not made virtual in base class it will be called rather than the Derived version of it

--------------------------------------------------------------------------------------------------------------

2) Destructor of base class should be always make virtual.
https://www.geeksforgeeks.org/virtual-destructor/

class Base{
public:
	virtual ~Base();
}

class Derived : public Base{
public:
	~Derived();
}

Base *p = new Derived();
delete p // calls ~Derived() first and than ~Base() ; reverse of constructor calling order

--------------------------------------------------------------------------------------------------------------

3) You cant apply virtual keyword infront of a constructor. Base class constructor is always called followed by derived
class constructor

another way of looking into is to see the below post which uses "factory method design pattern" to simulate the virtual
constructor

https://www.geeksforgeeks.org/advanced-c-virtual-constructor/


--------------------------------------------------------------------------------------------------------------------

4) Operator overloading. 

http://www.java2s.com/Tutorial/Cpp/0100__Development/Allocateanarrayofobjectsbyoverloadingnewanddeleteoperators.htm

Remember the syntax :

Following will override the global version, better to do for class specific version so define these as class memebers.

void* operator new(size_t size);
void operator delete(void*);
void *operator new[] (size_t);      // for arays
void operator delete[] (void *);

Implement your own new delete and array new and array delete

class Complex 
{
public:
  Complex (double a, double b): r (a), c (b) {}
  void* operator new(size_t size) { // overload
    return gMemoryManager.allocate(size);
  }
  void operator delete(void* pointerToDelete){
    gMemoryManager.free(pointerToDelete);
  } //overload 
private:
  double r; // Real Part
  double c; // Complex Part
};

* Passing and returning object from function overhead?
----------------------------------------------------------

rvalue : temporary (function return value) or calling a function and giving argument as move(resource)

lvalue : actual memory which is observable to the client, you can't move a resource out of lvalue as it is observable. 

#0 When returning a object or a container RVO (return value optimization) is invoked. 
    This implies No move ctor or copy constructor will be called. (neither of container nor of underlying objects). This implies you are building things directly in the caller variable/stack. most of the compiler does this optimization
    so in this case on move ctor / copy ctor call is expected. even in cases when we are are calling foo(goo()) of there a return object created inside a goo it can be just created as a argument of foo() so ctor is only called once
    when the object is created inside goo(). This rule superced anything other rule below. Not all compiler implement RVO to the fullest so you will see the called invoked using hte following rules (if RVO is no used to the fullest)

#1 When you are passing an object which is **rvalue** (temporary or by saying move() in the caller) to a function by value (not by reference). You will see the move ctor called.
 
#2 If you are passing a container(say vector) by value,which in itself is a lvalue (not temporary).You will see the underlying class copy ctor called. This scenerio should be avoided due to overhead.

#3 If you are passing a function, rvalue container (say by calling a function who returns a container). The conatainer move ctor is called (if RVO didnt happended). But underlying object ctor/copy ctor/ move ctor will not be called
   Hence even if we have to call a move ctor in case of vector  objects, we will only call the move ctor of vector not the underlying objects!. this will save time and no unnecessary copy will happen.

More detail in file ctor_copy_ctor_move_ctor.cpp


5) Move and copy semantic in C++?
----------------------------------
class members

* constructor // initilizing object with a differnet object.
* copy constructor // passing values in function where the argument is lvalue
* move constructor // passing values in function where the argument is rvalue
* assignment operator // copying value from one object to other

class string {
	char* data;
public:
	string (const char *p); //constructor
	string (const string & r) //copy constructor used in case we are calling function where argument is l value
	string (string && r); //move constructor, used in case we are calling function where argument is r value or explicit move() is called . No **const** in front of string. MOVE CTOR
	string& operator =(string rhs){ // assignment operator. no CONST and REF. move CTOR will be called.
		std::swap(data, rhs.data);
        return *this;
	}
}
if you do 
a = b; // when b is passed to the operator = function, a copy of b is made in rhs var, by calling "copy constructor".
	   now the rhs data is swapped to this.data 
a = x+z // move constructor is called when we initialize the rhs (because x+y is rvalue)


https://stackoverflow.com/questions/3106110/what-are-move-semantics

string s1 = "abc"; //ctor you are making a object from a diff object
string s2 = s1; // copy semantic a copy is made operator = called. s1--> argument (temp) --> swap the content with s2.
vs
string s1 = "abc";  //ctor you are making a object from a diff object
string s2 = move(s1); // explicit move symantic no copy is made s1 copy is moved to s2

vector<int> v1(v2); // copy semantic

vector<int> v1(move(v2)); // explicit move semantic. no copy. usually fast


6) Resource Management approaches : RAII (Resource Aquization is initialization) or SBRM (Scope based resource Management)
--------------------------------------------------------------------------------------------------------------------------

The basic idea is simple, if you want to have a resource (can be file/mutex/memory) don't use raw resouce. Wrap it around 
a class, in the class destructor clean up the resouce. This avoid memory leak.

http://www.acodersjourney.com/2016/05/top-10-dumb-mistakes-avoid-c-11-smart-pointers/

smart pointer
weak pointer
shared_ptr

Q: Build your own smart pointer?

#include<iostream>
using namespace std;
 
class SmartPtr
{
   int *ptr;  // Actual pointer
public:
   // Constructor: Refer https://www.geeksforgeeks.org/g-fact-93/
   // for use of explicit keyword 
   explicit SmartPtr(int *p = NULL) { ptr = p; } 
 
   // Destructor
   ~SmartPtr() { delete(ptr); }  
 
   // Overloading dereferencing operator
   int &operator *() {  return *ptr; }
};
 
int main()
{
    SmartPtr ptr(new int());
    *ptr = 20;
    cout << *ptr;
    //We don't need to call delete ptr: when the object ptr goes out of scope, destructor for it is automatically called and destructor does delete ptr.
 
    return 0;
}

----------
auto_ptr [ Depricated ] 
----------
only use for single dim object. Should not be used for array
auto_ptr is deprecated under C++11 and completely removed from C++17

#include<memory>

auto_ptr<int> x(new int(10));
x.get(); // get raw pointer
auto_ptr<int> y ;

* auto_ptr<X> p(new X[10]) // gives runtiem problem
* auto_ptr can't handle array type
auto_ptr<X[]> p(new X[10]) //syntax error
-------------------------------------

y = x; // move sementic. x reference to NULL
https://www.geeksforgeeks.org/smart-pointers-cpp/

https://www.geeksforgeeks.org/auto_ptr-unique_ptr-shared_ptr-weak_ptr-2/

https://mbevin.wordpress.com/2012/11/18/smart-pointers/

https://stackoverflow.com/questions/16711697/is-there-any-use-for-unique-ptr-with-array


unique_ptr
----------
should be used for array.

std::unique_ptr<int> p1(new int(5));
std::unique_ptr<int> p2 = p1; //Compile error.
std::unique_ptr<int> p3 = std::move(p1); //Transfers ownership. p3 now owns the memory and p1 is set to nullptr.

p3.reset(); //Deletes the memory.
p1.reset(); //Does nothing.

Basic understanding of how to deal with resouce leak and how to deal with them, Even if C++ does not have a garbage collector.

unique_ptr:
-------------
unique_ptr<X> up1(new X())
//unique_ptr<X> up1(up2); THIS WILL NOT WORK COMPILE ERROR
unique_ptr<X> up1 = move(up0);


up1.reset() //EXPLICIT FREE
up1.get() // gets the raw pointer

up1->foo() will invoke class X functions.
up1.xyz() will be the unique_ptr class wrapper functions.


shared_ptr:
-------------

shared_ptr<X> sh1(new X())
shared_ptr<X> sh2(sh1); THIS WILL NOT WORK COMPILE ERROR

sh1.reset() //EXPLICIT free reduce the ref cnt will not destroy the object X
sh2.get() // gets the raw pointer

sh2->foo() will invoke class X functions.
sh2.xyz() will be the unique_ptr class wrapper functions.


------------------------------------
unique_ptr and shared_ptr inside class member
https://stackoverflow.com/questions/15648844/using-smart-pointers-for-class-members

https://www.geeksforgeeks.org/auto_ptr-unique_ptr-shared_ptr-weak_ptr-2/




-------------------------------------------------------------------------------------------------------------

7) Can i make the destructor private? What will happen?

Compilation error if you want to create stack objects.
Error if you create object in heap and want to delete them
No error if you create object in heap but never called delete on them

So sure you can make destructor private without hitting compilation error in certain cases. But for most general 
cases this is a bad idea

https://www.geeksforgeeks.org/private-destructor/

8) Can i make the constructor private? What will happen?

Certainly you can do this (This will not result in compilation error by itself). 
this is what the singleton pattern do. In these cases you essentially provide a mechanism 
to create a object. This can be having a public static member function or a friend function creating an object for you.

https://www.geeksforgeeks.org/can-constructor-private-cpp/

