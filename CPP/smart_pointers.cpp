/* 

This file give a basic understanding of how to deal with resouce leak and how to deal with them, Even if C++ does not have a garbage collector.

unique_ptr:
-------------
unique_ptr<X> up1(new X())
//unique_ptr<X> up1(up2); THIS WILL NOT WORK COMPILE ERROR
unique_ptr<X> up1 = move(up0);


up1.reset() //EXPLICIT FREE
up1.get() // gets the raw pointer

up1->foo() will invoke class X functions.
up1.xyz() will be the unique_ptr class wrapper functions.


shared_ptr:
-------------

shared_ptr<X> sh1(new X())
shared_ptr<X> sh2(sh1); THIS WILL NOT WORK COMPILE ERROR

sh1.reset() //EXPLICIT free reduce the ref cnt will not destroy the object X
sh2.get() // gets the raw pointer

sh2->foo() will invoke class X functions.
sh2.xyz() will be the unique_ptr class wrapper functions.


*/


#include<memory>
#include<iostream>
#include<vector>


using namespace std;
class Y{
  int val;
public:
  Y(int i):val(i){cout<<"constructor Y"<< val <<endl;}
  Y(){cout<<"constructor Y"<< val <<endl;}
  ~Y(){cout<<"Distructor Y"<< val << endl;}
  void do1(){cout<<"Do"<<endl;}
};


class X{
  int val;
public:
  X(int i):val(i){cout<<"constructor X"<< val <<endl;}
  X(){cout<<"constructor X"<< val <<endl;}
  X(const X &a) {cout<<"copy constructor X"<< val <<endl;}
  X(X&& a) {cout<<"move constructor X"<< val <<endl;} // Notice no const move the move ctor
  ~X(){cout<<"Destructor X"<< val << endl;}
  X operator = (X a){ cout<<"Assignment operator"<<endl; return a;} // notice i am using pass by value (not pass by const val or ref. move ctor will be called
  void do1(){cout<<"Do"<<endl;}
};

void auto_and_unique_ptr(){
  X a(1);
  //auto_ptr<X> p(new X(2)); // DEPRICATED IN C++11
  unique_ptr<X[]> pu(new X[10]); // OK to use for array object
  unique_ptr<X> up1;
  unique_ptr<X> up2(new X(121));
  up1 = move(up2);
  up1.reset();
  //unique_ptr<X> up3(up1); //DOES NOT WORK !! ERROR
  //up1 = up2 //ERROR


  //shared ptr
  shared_ptr<X> sp1;
  shared_ptr<X> sp2(new X(11));
  sp1 = sp2; // OK REFCOUNT WILL INCREASE
  cout<< "RefCount:" << sp1.use_count() <<endl; // NOTICE use of . instead of ->
  

  //auto_ptr<X> p1(new X[5]); //DONT USE FOR ARRAY OBJECT RUNTIME CRASH
  //auto_ptr<X[]> p1(new X[5]); //syntax error

}




main(){
  auto_and_unique_ptr();
}

  
