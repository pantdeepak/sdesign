#include<unistd.h>
#include<malloc.h>
#include<iostream>
using namespace std;
class IMemoryManager 
{
public:
  virtual void* allocate(size_t) = 0;
  virtual void   free(void*) = 0;
};
 
class MemoryManager : public IMemoryManager{
  struct Header
  { 
    int size;
    Header *next;
  }; 

  Header* freelistStart;

  void * moremem(int nu){
    cout<<"Allocating more mem" << nu <<endl;
    // if we are here we don't find the match in the list. Allocate more.
    char* cp = (char *) sbrk(nu * sizeof(Header));
    if(cp == NULL) return cp;
    Header* up = (Header*) cp;
    up->size = nu;
    return (void*) up;
  }

public:
  MemoryManager () { 
    freelistStart = (Header*) malloc(sizeof(Header));
    freelistStart->size = 0;
    freelistStart->next = freelistStart;
  }
  virtual ~MemoryManager () { 
    
  }
  virtual void free(void *mem){
  }
  virtual void* allocate(size_t size){
    int nunits = (size + sizeof(Header))/sizeof(Header) + 1 ;

    Header* prevp = freelistStart;
    while(1){
      if (prevp->next->size == nunits || prevp->next->size == nunits+1 ){ //exact match or 1 extra return prevp->next
	prevp->next = prevp->next->next;
	Header *tmp = prevp->next;
	return (void*)(tmp + 1);
      }
      if(prevp->next->size > nunits+1){ // We have leftover
	prevp->next->size -=  nunits; // give it from tail end
	return (void*) (prevp->next + prevp->next->size);
      }
      if(prevp->next == freelistStart){ // wrapped around get more memory 
	void* p;
	if(( p = moremem(nunits)) == NULL) // NO memory left
	  return NULL;
	Header *tmp = prevp->next;
	prevp->next = (Header*) p;
	prevp->next->next= tmp;
      }else{
	prevp = prevp->next;
      }
    
    }
  
  }

};

MemoryManager gMemoryManager;

class Complex 
{
public:
  Complex (double a, double b): r (a), c (b) {}
  void* operator new(size_t size) { // overload
    return gMemoryManager.allocate(size);
  }
  void   operator delete(void* pointerToDelete){
    gMemoryManager.free(pointerToDelete);
  } //overload 
private:
  double r; // Real Part
  double c; // Complex Part
};



int main(int argc, char* argv[]) 
{
  int num = 1000;
  Complex* array[num];
  for (int i = 0;i  <  1000; i++) {
    for (int j = 0; j  <  num; j++) {
      array[j] = new Complex (i, j);
    }
    for (int j = 0; j  <  num; j++) {
      delete array[j];
    }
  }
  return 0;
}
