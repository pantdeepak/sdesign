** System Design category **

** Allocators **

SLOB  (K&R style.. traditional UNIX, memory efficient but not cache friendly). NO internal fragmentation but external fragmentation problem.
This is rather slow system. Take a lot of time to merge the free segment. Some advance SLOB system have multiple queue / list for various size
requests.

SLAB   (Take space from fixed size chunks.. can have internal fragmentation but no external fragmentation) if the objects are of same size hopefully no 
       internal fragmentation

SLUB ( Currently used in linux 2008 onwards ) debugability support , advanced version of SLAB

Usually used by kernel (linux). kmalloc() api

Useful when you have fixed size objects to be allocate (kernel have lots of them). Allocate memory in one continuous "physical chunk". No MMU needed for translation (unlinke malloc/user space memory)

http://www.secretmango.com/jimb/Whitepapers/slabs/slab.html

https://www.gotothings.com/unix/kmalloc-and-malloc-memory-allocation.htm

http://students.mimuw.edu.pl/ZSO/Wyklady/05_pamiec/5_pamiec_en.html

https://events.static.linuxfound.org/sites/events/files/slides/slaballocators.pdf

How to design a slab allocator?

How to design a allocator like malloc?


** VIDEOS **

SLOB/SLAB/SLUB

https://www.youtube.com/watch?v=h0VMLXavx30

https://www.youtube.com/watch?v=EWwfMM2AW9g

# Some consideration:

Which allocator is cache friendly?

What about if we have NUMA architecure? (Many processor sockets + Multiple RAM in a system), Which allocator is efficient in these cases?

SMP (mostly multicore desktops/laptops)

https://en.wikipedia.org/wiki/Symmetric_multiprocessing

As we grow # of processor in a system memory contention will slow down the whole system as we have shared memory (RAM) access bus. 
NUMA comes to rescue. You can consider it as cluster computing (many systems inside one computer system).

NUMA: (Mostly servers) - Linux already support NUMA architechure.

https://blogs.cisco.com/performance/process-and-memory-affinity-why-do-you-care

https://en.wikipedia.org/wiki/Non-uniform_memory_access

How linux handle NUMA related issues:

https://queue.acm.org/detail.cfm?id=2513149

*To see the slab allocation in linux system*

sudo cat /proc/slabinfo  | grep -E 'kmalloc|name'