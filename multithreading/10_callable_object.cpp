/* 

Various ways to pass a function to a thread.

+ global function 
+ functor (class object which act like a function  with a function operator())
+ any class function ( very useful) 
+ lambda function

use ref() to avoid making copy while calling thread func


*/
#include<iostream>
#include<thread>
#include<mutex>
#include<fstream>
#include<future>
using namespace std;

class Foo{
public:
  void f(int x ,int y) {cout<<"Function f called" <<endl ;}
  long g(double x) {return 0 ; }
  int operator()(int x) {cout<<"Functor func called" << endl; return 0;}
};

void foo(int x) {cout<<"Non class function" <<endl;}

int main(){
  Foo f;
  thread t1(f,10);// copy of f in a different thread
  thread t2(ref(f),10);// No copy  a() in a different thread
  thread t3(Foo(),10); // Temp Foo object  moved to thread
  thread t4([&](int x) { cout<< "lambda func" <<endl; return x*x ; }, 10);
  thread t5(foo, 10);  // non class member func
  thread t6(&Foo::f,f,10,'t'); // copy of a.f(10,'t')
  thread t7(&Foo::f,&f,10,'t'); // copy of a.f(10,'t')

  t1.join();t2.join();t3.join();t4.join();t5.join();t6.join();t7.join();
  //async(launch::async,f,10);
  //bind(f,10);
  //call_once(once_flag,f,10);
}
