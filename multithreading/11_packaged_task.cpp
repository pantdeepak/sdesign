/* 
 * You can create a packaged task and pass it to different function, thread or push it in queue for later processing by thread


 */
#include<iostream>
#include<thread>
#include<mutex>
#include<fstream>
#include<future>
using namespace std;


int factorial(int N){
  int res = 1;
  for(int i = N ; i>1 ; i--){
    res*=i;
  }
  cout<< "Result is:" <<res <<endl;
  return res;
}


int main(){
  
  cout<< "thread_id:" << this_thread::get_id() <<endl; // or t1.get_id()
  cout<< "# of parallel threads:" <<  thread::hardware_concurrency() <<endl;
  packaged_task<int(int)> t(factorial); // factorial takes int and return int. YOU CANT pass additional parameters in packaged tasked. if you need additional parameter you need to use bind() function
  packaged_task<int()> t2(bind(factorial,6)); //now the parameter is already buldled in the fucntion
  auto fo = bind(factorial,6); // why not to use just the bind to create the func object to execute it later. Why do we need the packaged_task? packaged_task link callable object to future!
  //..
  //..
  //..
  //..
  
  future<int> res = t.get_future();//get future of task

  thread task_td(move(t),5);  // execute task in different place (function) // MOVE is important here
  int x = res.get(); // get return val
  task_td.join();
  return 0;
}


