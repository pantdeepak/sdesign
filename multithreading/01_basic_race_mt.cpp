/* 
/*
0. Threading can be used in single system, Multiprocessing can be used across 
multiple systems (Distributed systems)

1. Thread can be joined or detached only once 

2. once detached forever detached, you can't rejoin

3  if thread t1 is neither joined nor detached before t1 goes out of scope the program will crash. so make sure each path including exception thrown have a t1.join(). Best way to handle this is make a RAII wrapper around t1 so when t1 goes out of scope t1 destructor is called which calles t1.join() in turn

t1.detach(); //t1 becomes a demon process

4 Parameter of thread is always passed by value, if you want to pass it by ref: use the ref() to do it. Or use pass by pointer or use the move() function

5. thread t2 = t1 will not compile. Use thread t2 = move(t1) 

6. this_thread::get_id() or t1.get_id() for getting the thread id 

7. cout is the common global object so race condition (jumblled up output will happen)
*/
#include<iostream>
#include<thread>

using namespace std;
void foo(){
  for(int i = 0 ; i < 10 ; i++)
    cout<<"Hello World"<<endl;
}

class Fctor{
public:
  void operator()(string msg){
    for(int i = 0 ; i < 10 ; i++){
      cout<<"From t1 " << msg <<" " <<  i <<endl;
    }
  }
};

/* class static member */
class Foo{
public:
  static void func(){cout<<"static class memember";}
  void bar(string s){ cout << s <<endl;}
};

int main_t1(){
  auto pid = this_thread::get_id();
  cout<< "thread_id:" << pid  <<endl; // or t1.get_id()
  cout<< "# of parallel threads:" <<  thread::hardware_concurrency() <<endl;
  
  thread t1(foo);
  Fctor fct;
  thread t2(fct,"this is arg"); // parameter always passed by value, use std::ref(s) , create the ref wrapper
  if(t1.joinable())
    t1.join(); // block main thread untill t1 is finished
  if(t2.joinable())
    t2.join();
  thread t3([&](){cout<<"lambda hello:PID:"<< pid <<endl;});
  t3.join();
  thread t4(Foo::func);
  t4.join();
  Foo fobj;
  thread t5(&Foo::bar,fobj,"non static member function");
  t5.join();
  return 0;
}

main(){
  main_t1();
  cout<<"abc"<<endl;
}

