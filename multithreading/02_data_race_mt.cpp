/* 

 cout is the common global object so race condition (jumbled up output will happen) will happen if lock is not used

mutex mu;
 use mu.lock() mu.unlock() to lock unlock critical section

Recommended way is to use RAII ( exception safe) 

lock_guard<mutex> guard(mu)

* Also define the critical section (function) part of a class. make mutex var private member of that class, you want to avoid other code using the mutex variable accidently (locking and unlocking it)

* Dont return the shared resouce from the class (you are loosing control of it)

*/
#include<iostream>
#include<thread>
#include<mutex>
using namespace std;
mutex mu; 

// Shared resource (cout) should be locked) when accessed by multiple threaded
void shared_print_simple_dont_use(string msg, int id){
  mu.lock();
  cout<<msg<< id << endl;
  mu.unlock();
}

//USE lock_guard exception safety!
// Also wrap around the shared_print in a class and make mu a private var
// mu should only be accessible by the shared_print() function

void shared_print(string msg, int id){
  std::lock_guard<mutex> guard(mu);
  cout<<msg<< id << endl;
}


using namespace std;
void foo(){
  //lock_guard<mutex>  guard(mu);
  for(int i = 0 ; i < 10 ; i++)
    shared_print(string("From foo") ,i);

}

int main(){
  cout<< "thread_id:" << this_thread::get_id() <<endl; // or t1.get_id()
  cout<< "# of parallel threads:" <<  thread::hardware_concurrency() <<endl;
  thread t1(foo);
  for(int i = 0 ; i < 10 ; i++)
    shared_print(string("From main") ,i);
    
  t1.join();

  return 0;
}


