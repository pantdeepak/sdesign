/* 

cout is the common global object so race condition (jumbled up output will happen) will happen if lock is not used.

the problem is cout is global and it is hard to lock it everywhere, Better way to organize the code is to do a file writer object which have file handle guarded by mutex

mutex mu;
 use mu.lock() mu.unlock() to lock unlock critical section

Recommended way is to use RAII ( exception safe) 

lock_guard<mutex> guard(mu)

* always make your shared resource wrapped inside a class and use a mutex (again as a private member of class)

*/
#include<iostream>
#include<thread>
#include<mutex>
#include<fstream>
using namespace std;



class LogFile{
  mutex m; // tight control wrapped in a class
  ofstream f; // SHARED RESOUCE !! NEVER RETURN f AS REFERENCE OR PASS TO A CALL BACK FUCNTION
public:
  LogFile(){
    f.open("log.txt");
  }
  ~LogFile(){ f.close();}
  
  void shared_print(string id, int v){
    lock_guard<mutex> g(m);
    f<< "From:" << id << ":" << v <<endl;
  }


};

using namespace std;
void foo(LogFile & log){
  for(int i = 0 ; i < 10 ; i++)
    log.shared_print("From foo:", i);
  
}

int main(){
  cout<< "thread_id:" << this_thread::get_id() <<endl; // or t1.get_id()
  cout<< "# of parallel threads:" <<  thread::hardware_concurrency() <<endl;
  LogFile log;
  thread t1(foo,ref(log));
  

  for(int i = 0 ; i < 10 ; i++)
    log.shared_print("from main",i);

    
  t1.join();

  return 0;
}


