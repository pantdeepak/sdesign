/* 

mutex mu;
 use mu.lock() mu.unlock() to lock unlock critical section

Recommended way is to use RAII ( exception safe) 

lock_guard<mutex> guard(mu)

OR 

unique_lock<mutex> locker(mu) is another way to do this with extra flexibility of locking and unlocking when you want

unique_lock is usualy slower than lock_guard() but give extra flexibility to lock and unlock when programmer want

* always make your shared resource wrapped inside a class and use a mutex (again as a private member of class)

call_once give a advantage to call the function only once! avoid overhead of making it critical section which all thread calls every time they enter

*/
#include<iostream>
#include<thread>
#include<mutex>
#include<fstream>
using namespace std;


/* suboptimal way of implementing lazy init in C++ in multithread programming env */
class LogFile{
  mutex m; // tight control wrapped in a class
  ofstream f; // SHARED RESOUCE !! NEVER RETURN f AS REFERENCE OR PASS TO A CALL BACK FUCNTION
  mutex lm; // lazy mutex
  once_flag _flag;
public:
  
  LogFile(){
    
  }
  ~LogFile(){ f.close();}
  
  void shared_print(string id, int v){

    /* This makes the wasting time in locking and unlocking */
    /*
    {
      lock_guard<mutex> lim(lm); // Notice that mutex is outside the if otherwise not thread safe ! THINK. 
      if(!f.is_open()){
	f.open("log.txt");
      }
    }
    */
    call_once(_flag,[&]{f.open("log.txt");});
    //lock_guard<mutex> g(m);
    unique_lock<mutex> g(m,std::defer_lock);
    g.lock(); // extra flexibility
    f<< "From:" << id << ":" << v <<endl;
    g.unlock(); // extra flexibility
    unique_lock<mutex> g2 = move(g); 
  }
};

void foo(LogFile & log){
  for(int i = 0 ; i < 10 ; i++)
    log.shared_print("From foo:", i);
  
}

int main(){
  cout<< "thread_id:" << this_thread::get_id() <<endl; // or t1.get_id()
  cout<< "# of parallel threads:" <<  thread::hardware_concurrency() <<endl;
  LogFile log;
  thread t1(foo,ref(log));
  

  for(int i = 0 ; i < 10 ; i++)
    log.shared_print("from main",i);

    
  t1.join();

  return 0;
}


