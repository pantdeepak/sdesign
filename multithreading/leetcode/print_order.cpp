#include<condition_variable>
#include<mutex>
#include<thread>
#include<iostream>
using namespace std;

class ZeroEvenOdd {
private:
    int n;
    
    mutex m;
    condition_variable cv;
    int turn=0;
    int num=1;

public:
    ZeroEvenOdd(int x) {
        this->n = x;
    }

    // printNumber(x) outputs "x", where x is an integer.
    void zero() {
        for(int i = 0 ; i <n; i++){
            unique_lock<mutex> ul(m);
            cv.wait(ul,[&](){return turn ==0;});
            cout<<"0";
            turn =1;
            cv.notify_all();    
        }
        
    }

    void even() {
        while(num<=n){
            unique_lock<mutex> ul(m);
            cv.wait(ul,[&](){return turn ==1 && num%2==0;});
            cout<<num;
            num++;
            turn =0;
            cv.notify_all();    
        }
        
    }

    void odd() {
        while(num<=n){
            unique_lock<mutex> ul(m);
            cv.wait(ul,[&](){return turn ==1 && num%2!=0;});
            cout<<num;
            num++;
            turn =0 ;
            cv.notify_all();    
        }
        
    }
};
int main(int argc, char** argv) {
  ZeroEvenOdd foo(2);

  thread t1(&ZeroEvenOdd::zero,ZeroEvenOdd(2));
  thread t2(&ZeroEvenOdd::even,ZeroEvenOdd(2));
  thread t3(&ZeroEvenOdd::odd,ZeroEvenOdd(2));
    t3.join();
    t1.join();
    t2.join();
    return 0;
}
