#include<iostream>
using namespace std;
#include<mutex>
#include<thread>
#include<vector>
mutex m;
vector<int> fibv(21,0);
void  fib(int n){
  if(n == 2){
     fibv[2] = 1;
     return;
  } 
  if(n==1){
    fibv[1] = 1;
    return;
  }
  thread t1(fib,n-1);
  thread t2(fib,n-2);
  t1.join();
  t2.join();
  unique_lock<mutex> lg(m);
  fibv[n] = fibv[n-1] + fibv[n-2];
  lg.unlock();
  return;
}

int main(){
  fib(20);
  cout << fibv[20];
}
