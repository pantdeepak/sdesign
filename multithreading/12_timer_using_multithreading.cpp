/* 
   A Timer class  where timer can be resetted.
   Also implement clock class which is a up counter

   Notice 
   1. There is no direct way (API) to kill a thread in C++. This is intentional as the OS does not know the exact state in which the thread is and kill it 
   will cause program to be in non-safe state

   2. This must be done using communication between threads conditiona_variable or futures

   3. We use lambda function here and you must pass a this to a lambda so that it have access to class member function

   this_thread::sleep_for(chrono::milliseconds(10)) is very handy to insert a delay

   Use time(nullptr) to get the time from epoch long int in second

*/

#include<mutex>
#include<thread>
#include<iostream>
#include<chrono>
#include<functional>
#include<ctime> // get current time in sec from epoch
#include<condition_variable>
using namespace std;

class Timer{
  condition_variable cond;
  int stillValid = 1;
  mutex m;
  mutex ctm;
  long int counterv;
public:
  //Timer(long int t):counterv(t){}
  void SetTimer(int ms){
    {
      lock_guard<mutex> l(m);
      stillValid = 1;
    }
    thread t([this,ms](){ // passing this is must for lambda to access the member func
	this_thread::sleep_for(chrono::milliseconds(ms));
	lock_guard<mutex> lg(m);
	if(stillValid)
	  CallBackFunc();
    
      });
    t.detach();
  }
  void StartUpCounter(long int sc){ // a clock 
    counterv = sc;
    cout<< sc <<endl;
    thread uct([this](){
	while(1){
	  lock_guard<mutex> ctl(ctm); // NOTE you can't have this lock outside the while. Otherwise deadlock
	  this_thread::sleep_for(chrono::seconds(1));
	  counterv++;;
	}
      });
    uct.detach();
  }
  void cancelUpCounter();
  void cancelTimer(){lock_guard<mutex> lg(m); stillValid = 0;}
  void CallBackFunc() {cout<<"Timer expired"<<endl ; }
  void printCurrentCounter(){lock_guard<mutex> ctl(ctm); cout<< counterv <<endl;}
};

int main(){
  Timer t;
  t.SetTimer(1000);
  t.cancelTimer(); // cancel it if you can
  this_thread::sleep_for(chrono::seconds(3)); // wait and see what happens
  t.SetTimer(1000); //set timer again
  t.StartUpCounter(time(nullptr));

  this_thread::sleep_for(chrono::seconds(10));
  t.printCurrentCounter();
  
}
