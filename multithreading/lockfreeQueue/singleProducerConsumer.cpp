#include<atomic>
#include<array>
#include<iostream>
#include<thread>
using namespace std;

/* 
Single producer and single consumer */
/* 
Requirements

1. NO data races
2. No allocation std:: queue cant be used
3. No locks (mutex.. condition_variable etc )

*/

// queue is empty R == W
// queue is not empty R != W 
// ==>  no data races 
// queue is full when W+1 == R. One cells is always empty in this model

// no allocations -allocation are not lock free(so queue have to have fixed capacity) 
// preallocated ring buffer 

/* 
   This is lock free and 
   wait free : At no point you have to say (inside queue implemenation) i cant do it right now and you have to re-try

 */

template<typename T,size_t size>
class Queue{
public:
  static constexpr size_t getPosAfter(size_t pos){
    return ++pos == ringBufferSize ? 0 : pos;
  }
  bool push(const T& newElement){
    auto oldWritePos = writePos.load();
    auto newWritePos = getPosAfter(oldWritePos);
   
    if(newWritePos == readPos.load()){
      cout<<newWritePos << " " << readPos.load() << "Queue full" <<endl;
       return false; // queue full
    }
    cout<<"push"<<endl;
    ringBuffer[oldWritePos] = newElement;
    writePos.store(newWritePos);
    return true;
  }
  bool pop(T& elementReturned){ //returned as elementReturned
    auto oldWritePos = writePos.load();
    auto oldReadPos = readPos.load();
    if(oldWritePos == oldReadPos) return false; // queue empty
    elementReturned = move(ringBuffer[oldReadPos]);
    readPos.store(getPosAfter(oldReadPos));
    return true;
  
  }
private:
  static constexpr size_t ringBufferSize = size+1;
  array<T,ringBufferSize> ringBuffer;
  atomic<size_t> readPos = {0};
  atomic<size_t> writePos = {0};
};

void producer(Queue<int,100> & lfq){
  cout<<"Inproducer thread" <<endl;
  for(int i = 0 ; i <99; i++){
    lfq.push(i);
  }
}
void consumer(Queue<int,100> &lfq){
  int v;
  int consumed = 0;
  cout<<"In Consumer thread" <<endl;
  while(consumed<99){
    if(lfq.pop(v)){
      cout<<v <<endl;
      consumed++;
    }
  }
}

// queue size have to 1 more than the actual element we want it to hold
main(){
  Queue<int,100> lfq;
  thread t1(producer,ref(lfq));
  thread t2(consumer,ref(lfq));
  t1.join();
  t2.join();
}
