#include<atomic>
#include<array>
#include<iostream>
#include<thread>
using namespace std;

/* 
Single producer and single consumer */
/* 
Requirements
1. NO data races
2. No allocations inside std:: queue cant be used
3. No locks (mutex.. condition_variable etc )

*/

// queue is empty R == W
// queue is not empty R != W 
// ==>  no data races 
// queue is full when W+1 == R. One cells is always empty in this model

// no allocations -allocation are not lock free(so queue have to have fixed capacity) 
// preallocated ring buffer 

/* 
   This is lock free and 
   But NOT wait free : We have a place where i say i cant do it right now and you have to re-try
   i.e following pattern is there 
   while(true){
      compare_and_exachnge_strong()
   }

 */



template<typename T,size_t size>
class Queue{
public:
  static constexpr size_t getPosAfter(size_t pos){
    return ++pos == ringBufferSize ? 0 : pos;
  }
  bool push(const T& newElement){
    auto oldWritePos = writePos.load();
    auto newWritePos = getPosAfter(oldWritePos);
   
    if(newWritePos == readPos.load()){
       return false; // queue full
    }
    ringBuffer[oldWritePos].store(newElement); // CHANGE
    writePos.store(newWritePos);
    return true;
  }
  //multi reader
  bool pop(T& elementReturned){ //returned as elementReturned
    auto oldWritePos = writePos.load();
    auto oldReadPos = readPos.load();
    if(oldWritePos == oldReadPos) return false; // queue empty
    while(true){ //retry loop
      elementReturned = ringBuffer[oldReadPos].load();
      if(readPos.compare_exchange_strong(oldReadPos,getPosAfter(oldReadPos)))
	return true;
      oldReadPos = readPos.load();
    }
    return true;
  
  }
private:
  static constexpr size_t ringBufferSize = size+1;
  array<atomic<T>,ringBufferSize> ringBuffer; //atomic<T> is important as writer can overwride the element when reader is trying to read it
  atomic<size_t> readPos = {0};
  atomic<size_t> writePos = {0};
};

void producer(Queue<int,100> & lfq){
  cout<<"Inproducer thread" <<endl;
  for(int i = 0 ; i <99; i++){
    lfq.push(i);
  }
}
void consumer(Queue<int,100> &lfq){
  int v;
  int consumed = 0;
  cout<<"In Consumer thread" <<endl;
  while(consumed<99){
    if(lfq.pop(v)){
      cout<<v <<endl;
      consumed++;
    }
  }
}

// queue size have to 1 more than the actual element we want it to hold
main(){
  Queue<int,100> lfq;
  thread t1(producer,ref(lfq));
  
  thread t2(consumer,ref(lfq));
  thread t3(consumer,ref(lfq));
  t1.join();
  t2.join();
  t3.join();
}
