/* 

Future and promise are ways to do async programming without having clumpsy syntax of lock_guard, condition_variable et al...

It tries to help us solve problem of  passing return values or arguments to function asynchronously 

What is the alternative?
  * You can pass a variable by reference to a  called function and by using thread() argument you can get the value back. but now you created a shared variable so  lock is needed and  use conitional_var to wait until the return value is available in the caller. This is complex

async() function
   launch::deferred. Does not create a new thread
   launch::async. Create a new thread
   


*/
#include<iostream>
#include<thread>
#include<mutex>
#include<fstream>
#include<future>
using namespace std;


int factorial(future<int> & f){
  int res = 1;
  int N = f.get(); // block here untill i send you something.
  for(int i = N ; i>1 ; i--){
    res*=i;
  }
  cout<< "Result is:" <<res <<endl;
  return res;
}


int main(){
  cout<< "thread_id:" << this_thread::get_id() <<endl; // or t1.get_id()
  cout<< "# of parallel threads:" <<  thread::hardware_concurrency() <<endl;

  /* how to pass a promise */
  promise<int> p;
  future<int> fa = p.get_future();

  int x ;
  //thread t1(factorial,4); // how do you get the value back from factorial function?

  //future<int> fv = async(factorial,4); /* may or may not create a thread ,implementation dependent */

  //future<int> fv = async(launch::deferred | launch::async ,factorial,4); // Equivalent to the API above

  future<int> fv = async(launch::async,factorial,std::ref(fa)); // definetly create a thread and pass a future

  p.set_value(4); // set the value of promise to send to subfunction. If not done program will throw promise broken exception
  
  x = fv.get(); // block here untill you get the results from factorial.  fu.get() can  only be called once.

  cout<< "Results from the caller:" << x << endl;

  return 0;
}


