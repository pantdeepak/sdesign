/* 

Deadlock:

What happens when we use 2 or more lock to lock some critical section?

if we are carefull everything should be fine but if we are sloppy deadlock can happen

Here the order of lock aquaring (m1,m2) is different in both shared_print and shared_print2, which causes the deadlock

Also noitce that deadlock is not always reproducable and will depend on how your thread race with each others.

What can we do to avoid this?
-----------------------------------

0. Avoid using multiple mutex for locking (if you can). Prefer locking single mutex

* Avoid locking mutex and calling a user provided function [ you never know if that func locks the mutex again]


1. Always lock mutex in same order. shared_print and shared_print2 have different lock order for m1 and m2. This constraint is very hard to enforce. What can be done better than this.

2. no easy way to say if you have deadlock in process. but 
ps aux | grep <program_name>  with Sl+ can give you a clue that the program is multithreaded and in Intrerruptable sleep state. Using Helgrind is a better way to deal with this.


*/
#include<iostream>
#include<thread>
#include<mutex>
#include<fstream>
#include<chrono>
using namespace std;

class LogFile{
  mutex m1; // tight control wrapped in a class
  mutex m2;
  ofstream f; // SHARED RESOUCE !! NEVER RETURN f AS REFERENCE OR PASS TO A CALL BACK FUCNTION
public:

  void shared_print(string id, int v){
    lock_guard<mutex> g1(m1);
    lock_guard<mutex> g2(m2);
    cout<< "From:" << id << ":" << v <<endl;
  }
  void shared_print2(string id, int v){
    lock_guard<mutex> g1(m2);
    this_thread::sleep_for(std::chrono::milliseconds(100)); // to simulate deadlock
    lock_guard<mutex> g2(m1);
    cout<< "From:" << id << ":" << v <<endl;
  }


};

void foo(LogFile & log){
  for(int i = 0 ; i < 10 ; i++)
    log.shared_print("From foo:", i);
  
}

void deadlock1(){
  cout<< "thread_id:" << this_thread::get_id() <<endl; // or t1.get_id()
  cout<< "# of parallel threads:" <<  thread::hardware_concurrency() <<endl;
  LogFile log;
  thread t1(foo,ref(log));

  for(int i = 0 ; i < 10 ; i++)
    log.shared_print2("from main",i);
  t1.join();

}
/* 
   use of lock() and std::adopt_lock to avoid deadlock
 */

class LogFile2{
  mutex m1; // tight control wrapped in a class
  mutex m2;
  ofstream f; // SHARED RESOUCE !! NEVER RETURN f AS REFERENCE OR PASS TO A CALL BACK FUCNTION
public:

  void shared_print(string id, int v){
    lock(m1,m2); // locks the mutex but in a way that avoid the deadlock!!
    // make sure both already-locked mutexes are unlocked at the end of scope
    lock_guard<mutex> g1(m1,std::adopt_lock);
    lock_guard<mutex> g2(m2,std::adopt_lock);
    cout<< "From:" << id << ":" << v <<endl;
  }
  void shared_print2(string id, int v){
    lock(m1,m2);
    // make sure both already-locked mutexes are unlocked at the end of scope
    lock_guard<mutex> g1(m2,std::adopt_lock);
    this_thread::sleep_for(std::chrono::milliseconds(100)); // to simulate deadlock
    lock_guard<mutex> g2(m1,std::adopt_lock);
    cout<< "From:" << id << ":" << v <<endl;
  }


};

void foo2(LogFile2 & log){
  for(int i = 0 ; i < 10 ; i++)
    log.shared_print("From foo:", i);
  
}

void deadlock2(){
  cout<< "thread_id:" << this_thread::get_id() <<endl; // or t1.get_id()
  cout<< "# of parallel threads:" <<  thread::hardware_concurrency() <<endl;
  LogFile2 log;
  thread t1(foo2,ref(log));

  for(int i = 0 ; i < 10 ; i++)
    log.shared_print2("from main",i);

    
  t1.join();
}

int main(){
  //deadlock1(); // Will deadlock
  deadlock2(); // deadlock avoidance using lock() function
  return 0;
}


