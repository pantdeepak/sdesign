#pragma once
#include<atomic>
#include<functional>
#include<mutex>
#include<thread>
#include<vector>
#include<list>
#include<condition_variable>
#include<iostream>
using namespace std;
using namespace std::chrono;
template<typename T>
/* a procuder consumer queue, get() blocks if there is no item in the queue */
class SynchronizedQueue{
public:
  SynchronizedQueue(){}
  void put(const T&data){
    unique_lock<mutex> lck(qm);
    queue.push_back(data);
    cv.notify_one();
  }
  T get(){
    unique_lock<mutex> lck(qm);
  
    cv.wait(lck,[&](){return !queue.empty();});
    T result = queue.front();
    queue.pop_front();
    return result;
   }
private:
  list<T> queue;
  mutex qm;
  condition_variable cv;
};
