/* dinning philosopher problem simulation and how to avoid deadlock */

#include<mutex>
#include<iostream>
#include<thread>
using namespace std;

class chopstick{
 public:
  mutex mu;
};

class dinningPhilosopher{
  int bites = 10;
  chopstick& left,&right;
  int id;
  mutex cmu;
public:
  
  dinningPhilosopher(chopstick& l,chopstick &r,int id):left(l),right(r),id(id){}
  bool pickup(){
    auto val = try_lock(left.mu,right.mu);
    if(val == -1){ // got the lock
      return true;
    }
    return false;
  }
  void putdown(){
    left.mu.unlock();
    right.mu.unlock();
  }
  void chew(){
    //lock_guard<mutex> lg(cmu); //This might again do deadlock ? NO
    //cout<<"DP:" <<id << " chewing..."<<endl;
  } 
  void eat(){
    while(!pickup()){} // aquire lock
    chew();
    putdown(); // release lock
   }
  void run(){
    for(int i = 0 ; i < bites; i++){
      eat();
    }
  }
};

#include<vector>

int main(){
  int const maxc = 111;
  chopstick *cs = new chopstick[maxc];
  dinningPhilosopher *dp[maxc];
  for(int dpi = 0 ; dpi<maxc; dpi++){
    int p = ((dpi-1>=0)?dpi-1:maxc-1);
    int a = (dpi+1)%maxc;
    dp[dpi] = new dinningPhilosopher(cs[p],cs[a],dpi); 
  }
  cout<<"hello"<<endl;
  vector<thread> vt;
  for(int i = 0 ; i<maxc; i++){
    vt.push_back(thread(&dinningPhilosopher::run,dp[i]));
  }
  for(int i=0; i< maxc; i++){
    vt[i].join();
  }
  
}
