/* 

C++ Containers are not threadsafe

the problem is cout is global and it is hard to lock it everywhere, Better way to organize the code is to do a file writer object which have file handle guarded by mutex

mutex mu;
 use mu.lock() mu.unlock() to lock unlock critical section

Recommended way is to use RAII ( exception safe) 

lock_guard<mutex> guard(mu)

* always make your shared resource wrapped inside a class and use a mutex (again as a private member of class)

* use conditional_varaiable for signal. Be careful of spurious wakeups 

cond.notify()
cond.notify_all()

cond.wait(mutex,lambdaFnForCheckingIfWakeUpisOK)

*/
#include<iostream>
#include<thread>
#include<mutex>
#include<fstream>
#include<stack>
#include<deque>
using namespace std;
#include <chrono>
#include<condition_variable>

// threadsafe stack but might not be exception safe

class ProducerConsumer{
  mutex m; // tight control wrapped in a class
  deque<int> q; // SHARED RESOUCE !! NEVER RETURN f AS REFERENCE OR PASS TO A CALL BACK FUCNTION
  condition_variable cond;
public:
  void Producer(){
    int count = 10;
    while(count >0){
      unique_lock<mutex> locker(m);
      q.push_front(count);
      locker.unlock();
      this_thread::sleep_for(chrono::milliseconds(50));
      count--;
    }
  }
  
  void Consumer(){ /* spinning */
    int data = 0 ;
    while(data !=1){
      unique_lock<mutex> locker(m);
      if(!q.empty()){
	data = q.back();
	q.pop_back();
	locker.unlock();
	cout<<"consumer got:" << data<<endl;
      }else{
	locker.unlock();
      }
    }
  }
  /* efficient implementation using cond var*/
  void ProducerWithCondVar(){ 
    int count = 10;
    while(count >0){
      unique_lock<mutex> locker(m);
      q.push_front(count);
      locker.unlock();
      cond.notify_one();
      this_thread::sleep_for(chrono::milliseconds(100));
      count--;
    }
  }
  /* 
    cond.wait() will block the consumer until it see the notify from consumer
    Also there are spurious wake, thats why we have lambda func !q.empty()
    if it is a spurious wake, the second cond will be false and consumer will
    go back to sleep again.
  */
  void ConsumerWithCondVar(){ /* spinning */
    int data=0;
    while(1){
      unique_lock<mutex> locker(m);
      cond.wait(locker,[&](){return !q.empty();});
      data = q.back();
      q.pop_back();
      locker.unlock();
      cout<<"consumer got:" << data<<endl;
    }
  }
  
  thread Producerthread(){
    return thread(&ProducerConsumer::Producer,this);
  }
  thread Consumerthread(){
    return thread(&ProducerConsumer::Consumer,this);
  }
  thread ProducerthreadWithCondVar(){
    return thread(&ProducerConsumer::ProducerWithCondVar,this);
  }
  thread ConsumerthreadWithCondVar(){
    return thread(&ProducerConsumer::ConsumerWithCondVar,this);
  }

  
};


int main(){
  cout<< "thread_id:" << this_thread::get_id() <<endl; // or t1.get_id()
  cout<< "# of parallel threads:" <<  thread::hardware_concurrency() <<endl;
  ProducerConsumer *pc = new ProducerConsumer();

  /* producer and consumer thread with busy waiting */
  
  thread t1 = pc->Producerthread();
  thread t2 = pc->Consumerthread();
  t1.join();
  t2.join();
  /* producer consumer thread with cond var */
  
  t1 = pc->ProducerthreadWithCondVar();
  t2 = pc->ConsumerthreadWithCondVar();
  t1.join();
  t2.join();
    


}


