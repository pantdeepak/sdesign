/* 

C++ Containers are not threadsafe

the problem is cout is global and it is hard to lock it everywhere, Better way to organize the code is to do a file writer object which have file handle guarded by mutex

mutex mu;
 use mu.lock() mu.unlock() to lock unlock critical section

Recommended way is to use RAII ( exception safe) 

lock_guard<mutex> guard(mu)

* always make your shared resource wrapped inside a class and use a mutex (again as a private member of class)

*/
#include<iostream>
#include<thread>
#include<mutex>
#include<fstream>
#include<stack>
using namespace std;
#include <chrono>


// threadsafe stack but might not be exception safe

class stackTS{
  mutex m; // tight control wrapped in a class
  stack<int> s; // SHARED RESOUCE !! NEVER RETURN f AS REFERENCE OR PASS TO A CALL BACK FUCNTION
public:
  // threadsafe but not exception safe. What happen if exception is thrown after pop() is called
  // but the values is not stored in the calling function. out of mem exception
  int pop(){
    lock_guard<mutex> g(m); // MAKE THREADSAFE
    int tope = s.top();

    s.pop();
    return tope;
  }
  void push(int i){
    lock_guard<mutex> g(m); // MAKE THREADSAFE
    s.push(i);
  }
  bool empty(){
    return s.empty();
  }
  void print_stack(){
    while(!empty()){
      int sv = pop();
      cout<< sv << " ";
    }
    cout<<endl;
  }

};

void foo(stackTS & s){
  for(int i = 100 ; i < 200 ; i++){
    s.push(i);
    this_thread::sleep_for(std::chrono::milliseconds(10));
  }
}

int main(){
  cout<< "thread_id:" << this_thread::get_id() <<endl; // or t1.get_id()
  cout<< "# of parallel threads:" <<  thread::hardware_concurrency() <<endl;
  stackTS stk;
  thread t1(foo,ref(stk));

  for(int i = 0 ; i < 100 ; i++){
    this_thread::sleep_for(std::chrono::milliseconds(10));
    stk.push(i);
  }
  
    
  t1.join();
  stk.print_stack();
  return 0;
}


