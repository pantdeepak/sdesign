#pragma once
#include<future>
#include "synchronized_queue.h"
void doNothing(){ cout<<"noop" <<endl;}
class ThreadPool{
private:
  atomic<bool> done;
  int threadcnt;
  SynchronizedQueue<function<void()> > work_queue;
  vector<thread> threads;
  void worker_thread(){
    while(!done)
      work_queue.get()();
   
  }
public:
  ThreadPool( int nt) : done(false){
    if(nt <=0)
      threadcnt = thread::hardware_concurrency();
    else
      threadcnt = nt;
    for(unsigned int i =0; i<threadcnt ;i++){
      threads.push_back(thread(&ThreadPool::worker_thread,this));
    }
    cout<<"ctor done"<<endl;
  }
  void pushtask(function<void()> func){work_queue.put(func);}
  ~ThreadPool(){
    cout<<"destructor called"<<endl;
    done = true;
    for(unsigned int i =0; i< threadcnt; i++)
      pushtask(&doNothing);
    for(auto &th: threads){
      if(th.joinable())
	th.join();
     }
  }
  
};
void calcprod(){
  cout<<"hello" <<endl;
}
main(){
  ThreadPool tp(2);
  //packaged_task<void()> pt(bind(calcprod));
  for(int i=0; i<100; i++)
    tp.pushtask(calcprod);
  this_thread::sleep_for(seconds(1));
  cout<<"x"<<endl;
}
