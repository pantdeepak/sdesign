#include<iostream>
using namespace std;
#include<mutex>
#include<thread>
#include<vector>
#include<future>
/* 
lessions.
   1. it is not desirable to genearete thread inside the recursive subroutine. you will run out of thread if the recursion run too deep. I ran out of thread in office machine when # of new thread created is 10K 

2. if you call only recursive routine as a thread in main its OK [ no thread creation per call of the function ]
but it does not return a value. you need to use future or promise to get the return value

3. 
its is not a good idea to generate more threads than the core. so its totally absurd and waste (poor performance) to generate 1 thread per function call.

4. read about the naive threaded version of qsort
https://stackoverflow.com/questions/16260879/how-to-parallelize-a-divide-and-conquer-algorithm-efficiently

Again not a good idea to tie your fucntion with the thread.

5. If you problem fits divide and conquer. each part is independent so different thread can work on that wihout lock. 
so divide sequentially (or parallelly if possible)
conquer with different thread without any locks

6. Fibonacii does not fit the paradiem of multithreading. Each result is dependent of output of previous result and you can't partition it in independent task so applying multithreading here is not going to yield much

https://softwareengineering.stackexchange.com/questions/238729/can-recursion-be-done-in-parallel-would-that-make-sense

IMPORTANT

queue based appraoch is better than recursion for multithreading, multiple recursive call to fibonacii need to communicate the value to the parent so you have to aquire lock and have global variable. thread() does not return value.


Recursive --> Iterative (Queue based) --> multithreading


*/


mutex m;
vector<int> fibv(40,0);


void  fib(int n){
  if(n == 2){
     fibv[2] = 1;
     return;
  } 
  if(n==1){
    fibv[1] = 1;
    return;
  }
  thread t1(fib,n-1);
  thread t2(fib,n-2);
  t1.join();
  t2.join();
  unique_lock<mutex> lg(m);
  fibv[n] = fibv[n-1] + fibv[n-2];
  lg.unlock();
  return;
}



int  fibf(int n){
  if(n == 2){
     return 1;
  } 
  if(n==1){
    return 1 ; 
  }
  cout<<this_thread::get_id()<<endl;
  future<int> ret = async(launch::async,&fibf,n-1);
  future<int> ret2 = async(launch::async,&fibf,n-2);
  return ret.get() + ret2.get();
  
}



int main(){
  cout<< fibf(30);
}



