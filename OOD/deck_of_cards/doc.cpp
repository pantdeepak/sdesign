#include <vector>
#include <string>
#include<algorithm>
using namespace std;

enum Suit{
  CLUB,
  Diamond,
  HEART,
  SPADE
};
enum Color{
  RED,
  BLACK
};

class Card{
  Suit s;
  string face;
  Color c;

public:
  Card(Suit sp, string fp):s(sp),face(fp){}
  virtual vector<int> getValue()=0;
  string getFace(){return face;}
  Suit getSuit(){return s;}
  Color getColor(){return c;}
	  
};

class BJCard:public Card{

public:
  
  BJCard(Suit sp, string fp):Card(sp,fp){}
  vector<int> getValue(){
	vector<int> ret;
	if(getFace() == "A"){
	  ret = {1,11};
	  return ret;
  	}
	if(getFace().size() == 1 && isalpha(getFace()[0])){
	  ret.push_back(10);
	  return ret;
	}else{
	  ret.push_back(getFace()[0] -'0');
	  return ret;
	}
  }

};

class Deck{
  vector<Card*> d;
public:
  Deck(){
	for(int s = 0 ; s < SPADE ; s++){
		for(int i = 1 ; i <=13; i++){
		  string face;
		  if(i == 1)face = 'A';
		  if(i ==11)face = 'J';
		  if(i ==12)face = 'Q';
		  if(i ==13)face = 'K';
		  Card *c = new BJCard(static_cast<Suit>(s),face);
		  d.push_back(c);
		}
	}
  }
  static int randomf(int j){
	return rand()%j;
  }
  void shuffle(){
	int nc = d.size();
	for(int i=0; i<nc; i++){
	  random_shuffle(d.begin(),d.end(),randomf);
	}
	
  }

  vector<Card*> getMultipleCards(int num){}
  
};

class Hand{
protected:
  vector<Card*> h;
  Deck *doc;
public:
  Hand(Deck* da,int initialNoOfCards):doc(da){
	h = doc->getMultipleCards(initialNoOfCards);
  }
  void AddCardsToHand(int num){
	vector<Card*> vc = doc->getMultipleCards(num);
	h.insert(h.end(),vc.begin(),vc.end());
  }

  virtual vector<int> score(); // sum of all cards in hand
  
  
};

class BJHand : public Hand {
	  
public:
  BJHand(Deck* da,int ncards):Hand(da,ncards){}
  vector<int> score(){
	int  cs = 0 ;
	for (int i= 0 ; i <h.size() ;i++){
		vector<int> valv = h[i]->getValue();
		cs = valv[0];
	}
  }
  
};

/* facade */

class Game{

protected:
  int np;
  Deck* dc;
  vector<Hand*> vh;
public:
  Game(int np):np(np){ dc = new Deck();}

};

class BJGame:public Game{

public:
  BJGame(int np):Game(np){}
  void DoOnePlayerMove() {}
  bool isGameOver(){
  }
  bool dealCard(){
	for(int i = 1 ; i <=np; i++){
	  vh.push_back(new BJHand(dc,2));
	}
  }
  int playGame(){
	dealCard();
	for(int i =0 ; i < np; i++){
	  DoOnePlayerMove();
	}
  }
};

int main(){


}
